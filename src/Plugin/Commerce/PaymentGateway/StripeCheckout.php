<?php

namespace Drupal\commerce_stripe_checkout\Plugin\Commerce\PaymentGateway;

use Drupal;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\PaymentMethodStorageInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_price\MinorUnitsConverterInterface;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\profile\Entity\ProfileInterface;
use Stripe\Balance;
use Stripe\Exception\ApiErrorException;
use Stripe\Exception\SignatureVerificationException;
use Stripe\PayZzmentIntent;
use Stripe\Stripe as StripeLibrary;
use Stripe\Webhook;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UnexpectedValueException;

/**
 * Provides the Stripe Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "stripe_checkout",
 *   label = @Translation("Stripe Checkout"),
 *   display_label = @Translation("Stripe Checkout"),
 *   modes = {
 *     "test" = @Translation("Test"),
 *     "live" = @Translation("Live"),
 *   },
 *   forms = {
 *     "add-payment-method" =
 *   "Drupal\commerce_stripe_checkout\PluginForm\Checkout\PaymentMethodAddForm",
 *     "offsite-payment" =
 *   "Drupal\commerce_stripe_checkout\PluginForm\Checkout\PaymentOffsiteForm",
 *   },
 *   requires_billing_information = FALSE,
 * )
 *
 */
class StripeCheckout extends OffsitePaymentGatewayBase implements StripeCheckoutInterface {

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('commerce_price.minor_units_converter'),
      $container->get('event_dispatcher'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, MinorUnitsConverterInterface $minor_units_converter, EventDispatcherInterface $event_dispatcher, LoggerChannelFactoryInterface $logger_channel_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time, $minor_units_converter);

    $this->eventDispatcher = $event_dispatcher;
    $this->logger = $logger_channel_factory->get('commerce_stripe_checkout');
    $this->init();
  }


  /**
   * Re-initializes the SDK after the plugin is unserialized.
   */
  public function __wakeup() {
    parent::__wakeup();

    $this->init();
  }

  /**
   * Initializes the SDK.
   */
  protected function init() {
    /*
        todo for now initializing the library is not necessary
        StripeLibrary::setAppInfo('Centarro Commerce for Drupal', '8.x-1.0-dev', 'https://www.drupal.org/project/commerce_stripe', 'pp_partner_Fa3jTqCJqTDtHD');

        // If Drupal is configured to use a proxy for outgoing requests, make sure
        // that the proxy CURLOPT_PROXY setting is passed to the Stripe SDK client.
        $http_client_config = Settings::get('http_client_config');
        if (!empty($http_client_config['proxy']['https'])) {
          $curl = new CurlClient([CURLOPT_PROXY => $http_client_config['proxy']['https']]);
          ApiRequestor::setHttpClient($curl);
        }

        StripeLibrary::setApiKey($this->configuration['secret_key']);
        StripeLibrary::setApiVersion('2020-08-27');
    */
  }

  /**
   * {@inheritdoc}
   */
  public function getPublishableKey() {
    return $this->configuration['publishable_key'];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'publishable_key' => '',
        'secret_key' => '',
        'webhook_secret' => '',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['publishable_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Publishable Key'),
      '#default_value' => $this->configuration['publishable_key'],
      '#required' => TRUE,
    ];
    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret Key'),
      '#default_value' => $this->configuration['secret_key'],
      '#required' => TRUE,
    ];

    $form['webhook_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webhook Secret Key'),
      '#default_value' => $this->configuration['webhook_secret'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      // Validate the secret key.
      $is_live = $values['mode'] === 'live';
      if (!empty($values['secret_key'])) {
        try {
          StripeLibrary::setApiKey($values['secret_key']);
          // Make sure we use the right mode for the secret keys.
          if (Balance::retrieve()
              ->offsetGet('livemode') !== $is_live) {
            $form_state->setError($form['secret_key'], $this->t('The provided secret key is not for the selected mode (@mode).', ['@mode' => $values['mode']]));
          }
        } catch (ApiErrorException $e) {
          $form_state->setError($form['secret_key'], $this->t('Invalid secret key.'));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['publishable_key'] = $values['publishable_key'];
      $this->configuration['secret_key'] = $values['secret_key'];
      $this->configuration['webhook_secret'] = $values['webhook_secret'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentSolution() {
    return $this->configuration['payment_solution'];
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    // todo
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    // todo
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    // todo
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    // todo
  }

  /**
   * {@inheritdoc}
   *
   * OnNotify returns different errors to Stripe's webhook
   * The front-end should check if
   * the flow is completed successfully
   *
   */
  public function onNotify(Request $request) {
    $webhook_secret =  $this->configuration['webhook_secret'];
    $signature = $request->headers->get('Stripe-Signature');
    $request_body = $request->getContent();

    try {
      try {
        $event = Webhook::constructEvent(
          $request_body, $signature, $webhook_secret
        );
      } catch (UnexpectedValueException $e) {
        return new JsonResponse($e->getMessage(), 406);
      } catch (SignatureVerificationException $e) {
        return new JsonResponse($e->getMessage(), 401);
      }

      if($event->type !== 'checkout.session.completed') {
        return new JsonResponse('No operations triggered by this event', 202);
      }


      $stripe_object = $event->data->object;

      if ($stripe_object->payment_status !== 'paid') {
        return new JsonResponse('Not paid yet', 400);
      }

      $stripe_metadata = $stripe_object->metadata;

      $orders = $this->entityTypeManager->getStorage('commerce_order')
        ->loadByProperties(['uuid' => $stripe_metadata->order_id]);

      if (empty($orders)) {
        return new JsonResponse('Order not found', 404);
      }

      /** @var OrderInterface $order */
      $order = reset($orders);

      if ($order->isPaid() && $order->getCompletedTime()) {
        return new JsonResponse('Order already processed', 202);
      }

      $paid_amount = Price::fromArray([
        // Conversion between Stripe and Commerce price values. todo is there a better way?
        'number' => $stripe_object->amount_total / 100,
        'currency_code' => $stripe_object->currency,
      ]);

      //todo: this checks a single payment, is necessary to consider incremental ones
      if ($order->getTotalPrice()->lessThan($paid_amount)) {
        return new JsonResponse('Payment not sufficient', 406);
      }

      $customer_details = $stripe_object->customer_details->toArray();
      $billing_profile = $this->updateBillingProfile($order, $customer_details);
      $order->setBillingProfile($billing_profile);

      $payment_method = NULL;
      if (!$order->get('payment_method')->isEmpty()) {
        /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
        $payment_method = $order->get('payment_method')->entity;
      }

      if (!$payment_method || $payment_method->getPaymentGatewayId() !== $this->parentEntity->id()) {
        $payment_method = $this->doCreatePaymentMethod($billing_profile, $order->getCustomerId());
        $payment_method->setReusable(FALSE);
        $payment_method->save();
        $order->set('payment_method', $payment_method);
      }

      $this->doCreatePayment($order, $payment_method, $paid_amount, $stripe_object->id);

      $message = 'order successfully payed';

      $transitions = $order->getState()->getTransitions();

      // The order may be already placed, by e.g. the front-end or the admin interface
      if (isset($transitions['place'])) {
        $order->getState()->applyTransitionById('place');
        $message = 'order successfully placed and payed';
      }

      $order->save();

      return new JsonResponse($message, 200);
    } catch (\Exception $e) {
      return new JsonResponse($e->getMessage(), 500);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    //todo actually not really needed? Is there a better interface to implements this?
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    //todo
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    //todo
  }


  protected function doCreatePaymentMethod(ProfileInterface $billing_profile, $customer_id) {
    $payment_method_storage = $this->entityTypeManager->getStorage('commerce_payment_method');
    assert($payment_method_storage instanceof PaymentMethodStorageInterface);

    return $payment_method_storage->createForCustomer(
      'stripe_checkout',
      $this->parentEntity->id(),
      $customer_id,
      $billing_profile
    );
  }

  protected function doCreatePayment(OrderInterface $order, PaymentMethodInterface $payment_method, Price $paid_amout, $remote_id) {
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $payment_storage->create([
      'state' => 'new',
      'amount' => $order->getBalance(),
      'payment_gateway' => $this->parentEntity->id(),
      'payment_method' => $payment_method->id(),
      'order_id' => $order->id(),
    ]);

    $payment->setRemoteId($remote_id);
    $payment->save();

    $payment->setAmount($paid_amout);
    $payment->setState('completed');
    $payment->setAuthorizedTime($this->time->getRequestTime());
    $payment->save();
  }

  protected function updateBillingProfile(OrderInterface $order, array $customer_details) {
    /** @var \Drupal\profile\Entity\ProfileInterface $billing_profile */
    $billing_profile = $order->getBillingProfile() ?: $this->buildCustomerProfile($order);
    $this->populateProfile($billing_profile, $customer_details);
    $billing_profile->save();

    return $billing_profile;
  }

  protected function buildCustomerProfile(OrderInterface $order) {
    return $this->entityTypeManager->getStorage('profile')->create([
      'uid' => $order->getCustomerId(),
      'type' => 'customer',
    ]);
  }

  protected function populateProfile(ProfileInterface $profile, array $customer_details) {
    //todo Stripe doesn't have a surname field, this is a very dirty workaround
    $address = $customer_details['address'];
    $split_name = explode(' ', $customer_details['name'], 2);
    $address['given_name'] = $split_name[0];
    $address['family_name'] = $split_name[1] ?? $split_name[0];

    $mapping = [
      'line1' => 'address_line1',
      'line2' => 'address_line2',
      'city' => 'locality',
      'country' => 'country_code',
      'postal_code' => 'postal_code',
      'state' => 'administrative_area',
      'given_name' => 'given_name',
      'family_name' => 'family_name',
    ];
    foreach ($address as $key => $value) {
      if (!isset($mapping[$key])) {
        continue;
      }
      $profile->address->{$mapping[$key]} = $value;
    }
  }

}
