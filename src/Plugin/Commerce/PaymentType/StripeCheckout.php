<?php

namespace Drupal\commerce_stripe_checkout\Plugin\Commerce\PaymentType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeBase;

/**
 * Provides the payment type for PayPal Checkout.
 *
 * @CommercePaymentType(
 *   id = "stripe_checkout",
 *   label = @Translation("Stripe Checkout"),
 * )
 */
class StripeCheckout extends PaymentTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    return [];
  }

}
