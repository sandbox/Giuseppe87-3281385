### Introduction

This project integrates Stripe Checkout with the Drupal Commerce payment and checkout systems.

### Requirements

This module requires the following:
* Submodules of Drupal Commerce package (https://drupal.org/project/commerce)
  - Commerce core
  - Commerce Payment (and its dependencies)
* Stripe PHP Library (https://github.com/stripe/stripe-php)
* Stripe Merchant account (https://dashboard.stripe.com/register)

### Configuration

* Create a webhook on [Stripe](https://dashboard.stripe.com/test/webhooks), listening for `checkout.session.completed`
* Add a new payment gateway at `admin/commerce/config/payment-gateways` of "Stripe Checkout" type
* You must have a decoupled front-end \ whatever service that handle the creation of and redirect of a Stripe Checkout Session.
* It's necessary to put inside the metadata of the Stripe Object the Drupal Order `uuid`, with the key `order_id`
* If everything works, when the user will complete the Stripe Checkout, the relative Drupal order will be placed and payed.

